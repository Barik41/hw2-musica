export const saveStateToLocalStorage = (key, state) => {
    window.localStorage.setItem(key, JSON.stringify(state));
}

export const getStateFromLocalStorage = (key) => {
    const lsvalue = window.localStorage.getItem(key);
    if (lsvalue) {
        return JSON.parse(lsvalue)
    }

    return null;
}
