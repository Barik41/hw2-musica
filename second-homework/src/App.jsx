import React, { useEffect, useState, memo } from 'react';
import Header from './components/Header/Header';
import ItemsContainer from './components/ItemsContainer/itemsContainer';
import ChoosenItems from './components/ChoosenItems/ChoosenItems';
import {saveStateToLocalStorage, getStateFromLocalStorage} from "./utils/localStorageFuncion"
import {CART_KEY, STAR_KEY} from "./constants"
import './App.scss';


function App() {

const [goods, setGoods] = useState([]);
const [choosenItems, setChosenItems] = useState([]);

const toggleFavourite = (id) => {
  setGoods((prev) => {
    const newState = [...prev]; 

    const index = newState.findIndex((item) => item.id === id) 
    if (index !== -1) { 
      newState[index].isFavourite = !newState[index].isFavourite 
      saveStateToLocalStorage(STAR_KEY, newState)
      return newState 
    } else { 
      saveStateToLocalStorage(STAR_KEY, prev)
      return prev 
    }
  })
};

const getData = async () => {
  try {
    const data = await fetch('./goods.json').then(res => res.json())
    setGoods(data)

    const starForm = getStateFromLocalStorage(STAR_KEY) 
    if(starForm) {
      setGoods(starForm)
    }
  } catch (err) {
    console.log(err);
  }
}

useEffect(() => {
  getData()
  
  const cartFroms = getStateFromLocalStorage(CART_KEY)
  if(cartFroms) {
    setChosenItems(cartFroms)
  }
}, [])


  return (
    <>
      <Header goods={goods} choosenItems={choosenItems} />
      <div className="container">

        <ItemsContainer items={goods} setChosenItems={setChosenItems} toggleFavourite={toggleFavourite}/>
        <ChoosenItems items={choosenItems}/>
  
      </div>
    </>
  )
}



export default memo(App);

